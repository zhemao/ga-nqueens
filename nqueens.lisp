(in-package :cl-user)

(defpackage #:nqueens
  (:use :cl :genalg)
  (:export #:solve))

(in-package :nqueens)

(defun ultimate (vec)
  (1- (length vec)))

(defun penultimate (vec)
  (- (length vec) 2))

(defun search-vertical (queens)
  (loop for i to (ultimate queens)
        counting (> (count i queens) 1)))

(defun occupied (queens r c)
  (= (elt queens r) c))

(defun count-diagonal-right (queens start)
  (loop for r to (ultimate queens)
        for c from start to (ultimate queens)
        counting (occupied queens r c)))

(defun count-diagonal-left (queens start)
  (loop for r to (ultimate queens)
        for c from start downto 0
        counting (occupied queens r c)))

(defun search-diagonal-right (queens)
  (loop for start to (penultimate queens)
        counting (> (count-diagonal-right queens start) 1)))

(defun search-diagonal-left (queens)
  (loop for start from 1 to (ultimate queens)
        counting (> (count-diagonal-left queens start) 1)))

(defun search-conflicts (queens)
  (+ (search-vertical queens)
     (search-diagonal-left queens)
     (search-diagonal-right queens)))

(defun random-board (n)
  (coerce
    (loop for i to (1- n) collect (random n))
    'vector))

(defun gen-init-population (n k)
  (loop for i to (1- k)
        collect (random-board n)))

(defun vector-range (start end)
  (coerce 
    (loop for i from start to (1- end) collect i)
    'vector))

(defun solve (n k)
  (evolve #'search-conflicts 
          (gen-init-population n k)
          (vector-range 0 n)))
