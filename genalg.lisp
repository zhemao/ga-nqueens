(in-package :cl-user)

(defpackage #:genalg
  (:use :cl)
  (:export #:evolve #:sample #:reproduce))

(in-package :genalg)

(defparameter *mutate-prob* 0.05)

(defun vector-max (vec)
  (loop for x being the elements of vec
        maximizing x))

(defun vector-random (vec)
  (elt vec (random (length vec))))

(defun gen-weights (scores)
  (let ((base (1+ (vector-max scores))))
    (map 'vector
      (lambda (x) (- base x))
      scores)))

(defun sample (weights population n)
  (let ((wmax (vector-max weights))
        (popvector (apply #'vector population))
        (beta 0)
        (index (random (length population))))
    (loop for i from 1 to n
          do (setf beta (+ beta (random (* 2.0 wmax))))
          collect (loop while (< (elt weights index) beta)
                        do (setf beta (- beta (elt weights index)))
                        do (setf index (mod (1+ index) (length popvector)))
                        finally (return (elt popvector index))))))

(defun mutate (child alphabet)
  (setf (elt child (random (length child)))
        (vector-random alphabet))
  child)

(defun reproduce (mother father alphabet)
  (let* ((n (length mother))
         (c (random n))
         (child (concatenate 'vector 
                             (subseq mother 0 c) 
                             (subseq father c))))
    (if (< (random 1.0) *mutate-prob*)
      (mutate child alphabet) child)))

(defun next-gen (parents alphabet)
  (if (null parents) nil
    (let ((mother (car parents))
          (father (cadr parents)))
      (cons
        (reproduce mother father alphabet)
        (next-gen (cddr parents) alphabet)))))

(defun evolve (fitness population alphabet)
  (let* ((scores (map 'vector fitness population))
         (solutionpos (position 0 scores)))
    (if solutionpos 
      (nth solutionpos population)
      (let* ((weights (gen-weights scores))
             (parents (sample weights population (* 2 (length population))))
             (newpop (next-gen parents alphabet)))
        (evolve fitness newpop alphabet)))))
