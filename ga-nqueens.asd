(in-package :cl-user)

(defpackage #:ga-nqueens-system
  (:use :cl :asdf))

(in-package :ga-nqueens-system)

(defsystem "ga-nqueens"
  :description "Solution to nqueens problem using genetic algorithm"
  :version "0.1.0"
  :author "Zhehao Mao <zhehao.mao@gmail.com>"
  :license "MIT"
  :components ((:file "genalg")
               (:file "nqueens")))
